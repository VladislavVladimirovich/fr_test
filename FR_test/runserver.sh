#!/bin/bash

cd /usr/src/app

python manage.py migrate
python manage.py cmd_createsuperuser ${SUPERUSER_NAME} ${SUPERUSER_PASS}
python manage.py set_status_send_messages
python manage.py set_time_zone
gunicorn --bind ${APP_HOST}:${APP_PORT} FR_test.wsgi