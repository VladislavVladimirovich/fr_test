from django.apps import AppConfig


class SendmessagelisttoclientsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'SendMessageListToClients'
    verbose_name = "Отправка сообщений"
