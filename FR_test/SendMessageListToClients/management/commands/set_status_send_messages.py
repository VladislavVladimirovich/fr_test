import json
from django.core.management.base import BaseCommand
from SendMessageListToClients.models import StatusSendMessage
from django.conf import settings


class Command(BaseCommand):
    help = 'Set status sending messages to database'

    def handle(self, *args, **options):
        path_file = settings.BASE_DIR.parent
        data_create = list()
        with open(f'{path_file}/data/data_to_db/list_status_send_messages.json', 'r') as f:
            list_status = json.load(f)
        db_time_zone = StatusSendMessage.objects.all().values_list('name', flat=True)
        for i in list_status:
            if i['name'] not in db_time_zone:
                data_create.append(StatusSendMessage(
                    id=i['id'], name=i['name'], description=i['description'], state=i['state'])
                )
        if data_create:
            StatusSendMessage.objects.bulk_create(data_create)
            return 'Statuses sending messages successful set'
        return 'Statuses sending messages no data to set'
