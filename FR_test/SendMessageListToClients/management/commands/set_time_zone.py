import json
from django.core.management.base import BaseCommand
from SendMessageListToClients.models import TimeZone
from django.conf import settings


class Command(BaseCommand):
    help = 'Set DB timezone'

    def handle(self, *args, **options):
        path_file = settings.BASE_DIR.parent
        data_create = list()
        with open(f'{path_file}/data/data_to_db/list_time_zone.json', 'r') as f:
            list_time_zone = json.load(f)
        db_time_zone = TimeZone.objects.all().values_list('name', flat=True)
        for i in list_time_zone:
            if i[0] not in db_time_zone:
                data_create.append(TimeZone(name=i[0], utcoffset=i[1]))
        if data_create:
            TimeZone.objects.bulk_create(data_create)
            return 'TimeZones successful set'
        return 'TimeZone no data to set'
