from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'create super user in cmd'

    def add_arguments(self, parser):
        parser.add_argument('username')
        parser.add_argument('password')

    def handle(self, *args, **options):
        username = options['username']
        password = options['password']
        if User.objects.filter(is_superuser=True).exists():
            return 'Superuser already exists!'
        user = User.objects.create_superuser(username=username, password=password)
        return f'pk: {user.pk}, username: {user.username}'
