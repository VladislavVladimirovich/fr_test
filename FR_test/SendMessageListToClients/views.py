import logging

from django.views.generic import TemplateView
from rest_framework import generics, viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView

from . import models, services
from .DRF import serializers


logger = logging.getLogger('django.request')


class Client(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer

    @staticmethod
    def statistics(request, *args, **kwargs):
        return Response({'status': 'successful'})


class Tag(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.Tag.objects.all()
    serializer_class = serializers.TagSerializer


class MobileCode(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.MobileCode.objects.all()
    serializer_class = serializers.SerializerMobileCode


class TimeZone(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.TimeZone.objects.all()
    serializer_class = serializers.SerializerTimeZone


class StatusSendMessage(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.StatusSendMessage.objects.all()
    serializer_class = serializers.SerializerStatusSendMessage


class Mailing(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer

    @staticmethod
    def statistics(request, *args, **kwargs):
        return Response(services.get_statistics())


class AllowedSendingTime(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = models.AllowedSendingTime.objects.all()
    serializer_class = serializers.AllowedSendingTimeSerializer


class Message(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.SerializerMessage
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['status_send__name', 'status_send_id']

    def get_queryset(self):
        if self.kwargs:
            return models.Message.objects.filter(**self.kwargs)
        return models.Message.objects.all()


class SetTimeZone(APIView):
    permission_classes = [IsAdminUser]

    @staticmethod
    def get(request, *args, **kwargs):
        return Response({'status_load': services.load_time_zone()})


class SetStatusSendMessage(APIView):
    permission_classes = [IsAdminUser]

    @staticmethod
    def get(request, *args, **kwargs):
        return Response({'status': 'successful' if services.create_status_message() else 'error'})


class StatusInfo(APIView):
    permission_classes = [IsAdminUser]

    @staticmethod
    def get(request, *args, **kwargs):
        return Response({'status_load': services.load_time_zone()})


class Auth(TemplateView):
    template_name = 'oauth.html'

    def get_context_data(self, **kwargs):
        context = super(Auth, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['status'] = 'logout'
            context['name'] = self.request.user.username
            context['superuser'] = 'OK' if self.request.user.is_superuser else 'NO'
        else:
            context['status'] = 'login'
        return context
