import re
from rest_framework import serializers
from SendMessageListToClients.models import TimeZone


def validator_time_zone_name(value: str) -> str:
    if not re.match('[a-zA-Z]+/[a-zA-Z]+', str(value)):
        raise serializers.ValidationError("Не верный формат, пример: <name:Europe/Moscow> или <id:1>")
    return value


def validator_time_zone(value: str) -> TimeZone:
    if isinstance(value, int):
        time_zone = TimeZone.objects.filter(pk=value).first()
        error_msg = f"Часовой пояс <id: {value}> не найден"
    elif isinstance(value, str):
        time_zone = TimeZone.objects.filter(name=value).first()
        error_msg = f"Часовой пояс <name: {value}> не найден"
    else:
        time_zone = None
        error_msg = f"Не верный тип <type: {type(value).__name__}> данных, <type:id:int> или <type:name:str>"

    if not time_zone:
        raise serializers.ValidationError({"time_zone": [error_msg]})

    return time_zone
