from rest_framework import serializers
from SendMessageListToClients.models import Tag


def validator_tag(value: str or int) -> Tag:
    if isinstance(value, int):
        result = Tag.objects.filter(pk=value).first()
        error_msg = f"Тег <id: {value}> не найден"
    elif isinstance(value, str):
        result = Tag.objects.get_or_create(name=value)[0]
        error_msg = None
    else:
        result = None
        error_msg = f"Не верный тип <type: {type(value).__name__}> данных, <type:id:int> или <type:name:str>"

    if not result:
        raise serializers.ValidationError({"Tag": [error_msg]})

    return result
