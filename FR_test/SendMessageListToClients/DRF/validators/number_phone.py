from rest_framework import serializers


def validator_number_phone(value: str) -> str:
    if not value[0] == '7' or not len(value) == 11 or not value.isdigit():
        raise serializers.ValidationError({"number": "Номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)"})
    return value
