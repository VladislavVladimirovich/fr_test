from rest_framework import serializers
from SendMessageListToClients.models import MobileCode


class SerializerMobileCode(serializers.ModelSerializer):
    class Meta:
        model = MobileCode
        fields = ('id', 'code')

    def create(self, validated_data):
        return MobileCode.objects.get_or_create(**validated_data)[0]