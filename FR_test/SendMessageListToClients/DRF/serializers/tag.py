from rest_framework import serializers
from SendMessageListToClients.models import Tag


class SerializerTag(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')

    def create(self, validated_data):
        return Tag.objects.get_or_create(**validated_data)[0]


class TagSerializer(serializers.ModelSerializer):
    def __init__(self, instance=None, **kwargs):
        self.instance = instance
        self.kwargs = self.__set_params(kwargs)
        super(TagSerializer, self).__init__(instance=self.instance, **self.kwargs)

    class Meta:
        model = Tag
        fields = "__all__"

    def create(self, validated_data):
        return Tag.objects.get_or_create(**validated_data)[0]

    def __set_params(self, kwargs):
        return kwargs
