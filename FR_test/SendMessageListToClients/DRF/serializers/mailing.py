from rest_framework import serializers
from SendMessageListToClients.models import Mailing


class MailingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mailing
        fields = "__all__"
