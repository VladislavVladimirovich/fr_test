from rest_framework import serializers
from SendMessageListToClients.models import Message


class SerializerMessage(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
