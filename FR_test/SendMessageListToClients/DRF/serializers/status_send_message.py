from rest_framework import serializers
from SendMessageListToClients.models import StatusSendMessage


class SerializerStatusSendMessage(serializers.ModelSerializer):

    class Meta:
        model = StatusSendMessage
        fields = '__all__'
