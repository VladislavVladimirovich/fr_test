from rest_framework import serializers
from SendMessageListToClients.models import Client, MobileCode

from .mobile_code import SerializerMobileCode
from .tag import TagSerializer
from .time_zone import SerializerTimeZone

from .. import validators


class ClientSerializer(serializers.ModelSerializer):
    def __init__(self, instance=None, **kwargs):
        self.instance = instance
        self.kwargs = self.__set_params(kwargs)
        super(ClientSerializer, self).__init__(instance=self.instance, **self.kwargs)

    mobile_code = SerializerMobileCode()
    tag = TagSerializer()
    time_zone = SerializerTimeZone()

    class Meta:
        model = Client
        fields = "__all__"

    def update(self, instance, validated_data):
        instance.tag_id = self.kwargs['data']['tag']['id']
        instance.time_zone_id = self.kwargs['data']['time_zone']['id']
        instance.save()
        return instance

    def create(self, validated_data):
        return Client.objects.create(
            number_phone=self.kwargs['data']['number_phone'],
            mobile_code_id=self.kwargs['data']['mobile_code']['id'],
            tag_id=self.kwargs['data']['tag']['id'],
            time_zone_id=self.kwargs['data']['time_zone']['id'],
        )

    def __set_params(self, kwargs: dict) -> dict:
        """ Установка параметров входящих данных в зависимости от запроса """
        if 'data' in kwargs.keys() and kwargs["context"]["request"].method == 'POST':
            # validator number
            number = validators.validator_number_phone(kwargs['data']['number_phone'])
            if not number[0]:
                raise serializers.ValidationError({"number_phone": [number[1]]})
            # validator time_zone
            time_zone = validators.validator_time_zone(kwargs['data']['time_zone'])
            mobile_code = MobileCode.objects.get_or_create(code=kwargs['data']['number_phone'][1:4])[0]
            tag = validators.validator_tag(kwargs['data'].get('tag'))
            kwargs['data']['mobile_code'] = {"id": mobile_code.id, "code": mobile_code.code}
            kwargs['data']['tag'] = {"id": tag.id, "name": tag.name}
            kwargs['data']['time_zone'] = {"id": time_zone.id, "name": time_zone.name}

        if 'data' in kwargs.keys() and kwargs["context"]["request"].method in ['PUT', 'PATCH']:
            kwargs['data'].update({"number_phone": self.instance.number_phone})
            kwargs['data'].update({"mobile_code": {
                "id": self.instance.mobile_code.id, "code": self.instance.mobile_code.code
            }})
            if kwargs['data'].get('tag'):
                tag = validators.validator_tag(kwargs['data'].get('tag'))
            else:
                tag = self.instance.tag

            if kwargs['data'].get('time_zone'):
                time_zone = validators.validator_time_zone(kwargs['data']['time_zone'])
            else:
                time_zone = self.instance.time_zone

            kwargs['data'].update({'tag': {"id": tag.id, "name": tag.name}})
            kwargs['data'].update({'time_zone': {"id": time_zone.id, "name": time_zone.name}})

        return kwargs
