import re
from rest_framework import serializers
from SendMessageListToClients.models import TimeZone
from .. import validators


class SerializerTimeZone(serializers.ModelSerializer):
    name = serializers.CharField(validators=[validators.validator_time_zone_name])

    class Meta:
        model = TimeZone
        fields = ('id', 'name')

    def create(self, validated_data):
        return TimeZone.objects.get_or_create(**validated_data)[0]
