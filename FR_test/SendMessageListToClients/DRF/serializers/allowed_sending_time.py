from rest_framework import serializers
from SendMessageListToClients.models import AllowedSendingTime


class AllowedSendingTimeSerializer(serializers.ModelSerializer):

    class Meta:
        model = AllowedSendingTime
        fields = "__all__"
