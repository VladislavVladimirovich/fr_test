from django.test import TestCase
from SendMessageListToClients.services.send_mail import *


BODY_TO_VALIDATOR = {
    "status": "successful",
    "data": [
        {
            "id": 37,
            "statistics": {
                "ErrorTimeOut": {
                    "data": [
                        {
                            "id": 91,
                            "date_time": "2022-08-28 01:08:46+03:00",
                            "client": {
                                "id": 1,
                                "number_phone": "79381504815",
                                "time_zone": {
                                    "id": 2,
                                    "name": "Europe/Moscow",
                                    "utcoffset": "+03:00"
                                },
                                "tag": {
                                    "id": 20,
                                    "name": "Test_tag_new"
                                },
                                "mobile_code": {
                                    "id": 1,
                                    "code": 938
                                }
                            }
                        }
                    ]
                },
                "Successful": {
                    "data": [
                        {
                            "id": 92,
                            "date_time": "2022-08-28 01:08:46+03:00",
                            "client": {
                                "id": 3,
                                "number_phone": "79381504819",
                                "time_zone": {
                                    "id": 1,
                                    "name": "Europe/Kaliningrad",
                                    "utcoffset": "+02:00"
                                },
                                "tag": {
                                    "id": 20,
                                    "name": "Test_tag_new"
                                },
                                "mobile_code": {
                                    "id": 1,
                                    "code": 938
                                }
                            }
                        }
                    ]
                },
                "count": 2
            },
            "date_time_start": "2022-08-28 01:08:46+03:00",
            "date_time_stop": "2022-08-28 01:08:58+03:00",
            "tag": [
                {
                    "id": 20,
                    "name": "Test_tag_new"
                }
            ],
            "mobile_code": [
                {
                    "id": 1,
                    "code": 938
                }
            ]
        }]}


class SendMailTestCase(TestCase):

    def setUp(self):
        mail_to = ' test_mait@test.ru , '
        mail_From = 'test_mait@test.ru'
        self.validator = ValidatorDataMessage(BODY_TO_VALIDATOR, mail_to, mail_From)
        self.validator.handle()

    def test_validator_error(self):
        self.assertEqual(0, len(self.validator.errors))

    def test_validator_email_from(self):
        self.assertEqual('test_mait@test.ru', self.validator.email_from)

    def test_validator_emails_to(self):
        self.assertEqual(['test_mait@test.ru'], self.validator.email_to)

    def test_validator_form_body(self):
        self.assertEqual(1, len(self.validator.body.get('data')))

    def test_validator_response(self):
        self.assertEqual(1, len(self.validator.body.get('data')))

