from datetime import timedelta

from faker import Faker
from django.utils import timezone
from django.test import TestCase

from SendMessageListToClients.services import GetMessageTo
from SendMessageListToClients.models import *
from SendMessageListToClients.services import create_status_message

class SetStatusMessageApiTestCase(TestCase):

    def setUp(self):
        # create client
        self.client_1 = self.__create_client()
        self.client_2 = self.__create_client()
        self.client_3 = self.__create_client()
        # create mailing
        self.mailing_1 = self.__create_mailing(self.client_1, data_start=timezone.localtime() + timedelta(seconds=300))
        self.mailing_2 = self.__create_mailing(self.client_1, data_start=timezone.localtime() + timedelta(seconds=300))
        self.mailing_3 = self.__create_mailing(self.client_2)
        self.mailing_4 = self.__create_mailing(self.client_2)
        self.mailing_5 = self.__create_mailing(self.client_3, use_client_data=False)
        # create status send
        create_status_message()

    def test_get_messages(self):
        messages = GetMessageTo()
        result = messages.get_message()
        instances_messages = Message.objects.all()
        self.assertTrue(4 == len(result) == instances_messages.count())
        self.assertTrue(instances_messages.filter(mailing_id=self.mailing_1.id).exists())
        self.assertTrue(instances_messages.filter(mailing_id=self.mailing_2.id).exists())
        self.assertTrue(instances_messages.filter(mailing_id=self.mailing_3.id).exists())
        self.assertTrue(instances_messages.filter(mailing_id=self.mailing_4.id).exists())
        self.assertFalse(instances_messages.filter(mailing_id=self.mailing_5.id).exists())

    def __create_client(self):
        tag, mobile_code = self.__create__tag_mobile_code()
        return Client.objects.create(
            number_phone=f'7{mobile_code.code}{Faker().random_number(digits=7, fix_len=True)}',
            tag=tag,
            time_zone=TimeZone.objects.get_or_create(name='Europe/Moscow')[0],
            mobile_code=mobile_code
        )

    @staticmethod
    def __create__tag_mobile_code():
        tag = Tag.objects.create(name=Faker().msisdn())
        mobile_code = MobileCode.objects.create(code=Faker().random_number(digits=3, fix_len=True))
        return tag, mobile_code

    def __create_mailing(self, client_instance: Client, data_stop=timezone.localtime() + timedelta(seconds=600), data_start=None, use_client_data: bool = True):

        text = Faker().paragraph(nb_sentences=5)
        data_start = data_start or timezone.localtime()
        data_stop = data_stop
        if use_client_data:
            tag = client_instance.tag
            mobile_code = client_instance.mobile_code
        else:
            tag, mobile_code = self.__create__tag_mobile_code()

        instance = Mailing(message_text=text, date_time_start=data_start, date_time_stop=data_stop)
        instance.save()
        instance.mobile_code.add(mobile_code)
        instance.tag.add(tag)
        instance.save()
        return instance
