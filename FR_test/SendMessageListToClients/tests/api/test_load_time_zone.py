from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status
from SendMessageListToClients.models import TimeZone


class LoadTimeZoneApiTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('super_user', 'super_user@user.com', 'super_user')
        self.test_user = User.objects.create(username='test_user', password='test_user')
        self.url_load_time_zone = reverse('load_time_zone')

        self.test_data = {"name": "test1/test_city"}
        self.test_data_create = {"name": "test/test_city"}
        self.test_data_update = {"name": "test/test_city_new_name"}

        self.test_data_instance = TimeZone.objects.create(**self.test_data)

        self.url_list = reverse('timezone-list')
        self.url_detail = reverse('timezone-detail', args=[self.test_data_instance.id])

    def test_get_no_auth(self):
        response = self.client.get(self.url_load_time_zone)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_user(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_load_time_zone)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_super_user(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_load_time_zone)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIsInstance(response.json(), list)

    def test_create(self):
        self.client.force_login(user=self.superuser)
        response = self.client.post(self.url_list, data=self.test_data_create, format='json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(self.test_data_create['name'], response.json().get('name'))

    def test_update(self):
        self.client.force_login(user=self.superuser)
        response = self.client.put(self.url_detail, data=self.test_data_update, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(self.test_data_update['name'], response.json().get('name'))

    def test_destroy(self):
        self.client.force_login(user=self.superuser)
        response = self.client.delete(self.url_detail)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
