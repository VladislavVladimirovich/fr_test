from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status


class SetStatusMessageApiTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('super_user', 'super_user@user.com', 'super_user')
        self.test_user = User.objects.create(username='test_user', password='test_user')

        self.url_list = reverse('set_status_message')

    def test_get_no_auth(self):
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_user(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_super_user_create(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual({'status': 'successful'}, response.json())
