from django.urls import reverse
from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework.test import APITestCase
from rest_framework import status
from SendMessageListToClients.models import Tag, MobileCode, Mailing
from SendMessageListToClients.DRF.serializers import MailingSerializer


class LoadTimeZoneApiTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('super_user', 'super_user@user.com', 'super_user')
        self.test_user = User.objects.create(username='test_user', password='test_user')

        self.test_data_tag = Tag.objects.create(name='test_tag')
        self.test_data_mobile_code = MobileCode.objects.create(code=903)

        self.test_data_instance = Mailing(message_text="test_text", date_time_stop=timezone.localtime())
        self.test_data_instance.save()
        self.test_data_instance.mobile_code.add(MobileCode.objects.create(code=933))
        self.test_data_instance.tag.add(Tag.objects.create(name='test_tag_update'))
        self.test_data_instance.save()

        self.test_data_update = MailingSerializer(self.test_data_instance).data
        self.test_data_update['message_text'] = "test_text_update"

        self.test_data_create = MailingSerializer(self.test_data_instance).data
        self.test_data_create['message_text'] = "test_text_create"

        self.url_list = reverse('mailing-list')
        self.url_detail = reverse('mailing-detail', args=[self.test_data_instance.id])

    def test_get_no_auth(self):
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_user(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_auth_super_user(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(MailingSerializer([self.test_data_instance], many=True).data, response.json())

    def test_create(self):
        self.client.force_login(user=self.test_user)
        response = self.client.post(self.url_list, data=self.test_data_create, format='json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual( self.test_data_create['message_text'], response.json().get('message_text'))

    def test_update(self):
        self.client.force_login(user=self.test_user)
        response = self.client.put(self.url_detail, data=self.test_data_update, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(self.test_data_update['message_text'], response.json().get('message_text'))

    def test_destroy(self):
        self.client.force_login(user=self.test_user)
        response = self.client.delete(self.url_detail)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
