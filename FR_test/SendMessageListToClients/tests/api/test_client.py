from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status
from SendMessageListToClients.models import Tag, TimeZone, MobileCode, Client
from SendMessageListToClients.DRF.serializers import ClientSerializer


class LoadTimeZoneApiTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('super_user', 'super_user@user.com', 'super_user')
        self.test_user = User.objects.create(username='test_user', password='test_user')

        self.data_client = {
            "number_phone": '79030000000',
            "tag": Tag.objects.create(name='test_tag'),
            "time_zone": TimeZone.objects.create(name='Europe/Moscow'),
            "mobile_code": MobileCode.objects.create(code=903)
        }

        self.data_client_create = {
            "number_phone": '79030000001',
            "tag": self.data_client['tag'].name,
            "time_zone": self.data_client['time_zone'].name,
            "mobile_code": self.data_client['mobile_code'].id,
        }

        self.test_data_update = {"tag": 'Europe/Moscow'}

        self.test_data_instance = Client.objects.create(**self.data_client)
        self.test_data_update = {
            "tag":  Tag.objects.create(name='test_update').id,
            "time_zone": TimeZone.objects.create(name='Europe/update').id
        }

        self.url_list = reverse('client-list')
        self.url_detail = reverse('client-detail', args=[self.test_data_instance.id])

    def test_get_no_auth(self):
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_user(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_auth_super_user(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(ClientSerializer([self.test_data_instance], many=True).data, response.json())

    def test_create(self):
        self.client.force_login(user=self.test_user)
        response = self.client.post(self.url_list, data=self.data_client_create, format='json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual('79030000001', response.json().get('number_phone'))

    def test_update(self):
        self.client.force_login(user=self.test_user)
        response = self.client.put(self.url_detail, data=self.test_data_update, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(self.test_data_update['tag'], response.json().get('tag').get('id'))
        self.assertEqual(self.test_data_update['time_zone'], response.json().get('time_zone').get('id'))

    def test_destroy(self):
        self.client.force_login(user=self.test_user)
        response = self.client.delete(self.url_detail)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
