from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status

from SendMessageListToClients.DRF.serializers import SerializerMessage
from SendMessageListToClients.models import (
    Message,
    StatusSendMessage,
    Mailing,
    Client,
    Tag,
    TimeZone,
    MobileCode
)


class LoadTimeZoneApiTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('super_user', 'super_user@user.com', 'super_user')
        self.test_user = User.objects.create(username='test_user', password='test_user')

        data_client = {
            "number_phone": '79030000000',
            "tag": Tag.objects.create(name='test_tag'),
            "time_zone": TimeZone.objects.create(name='Europe/Kaliningrad'),
            "mobile_code": MobileCode.objects.create(code=903)
        }
        self.test_data_client = Client.objects.create(**data_client)
        data_mailing = {
            "message_text": "test_text",
            "date_time_stop": timezone.localtime(),
        }

        self.test_data_mailing = Mailing(**data_mailing)
        self.test_data_mailing.save()
        self.test_data_mailing.mobile_code.add(self.test_data_client.mobile_code)
        self.test_data_mailing.tag.add(self.test_data_client.tag)

        data_status_message = {"name": "New_message", "description": "Новое сообщение", "state": False}
        self.test_data_status_send_message = StatusSendMessage.objects.create(**data_status_message)

        self.test_data_message = Message.objects.create(
            status_send=self.test_data_status_send_message,
            mailing=self.test_data_mailing,
            client=self.test_data_client,
        )
        self.url_list = reverse('message')

    def test_get_no_auth(self):
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_user(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_auth_super_user(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(SerializerMessage([self.test_data_message], many=True).data, response.json())
