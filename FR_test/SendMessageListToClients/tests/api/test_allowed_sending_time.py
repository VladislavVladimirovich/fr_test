import datetime
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status
from SendMessageListToClients.models import AllowedSendingTime
from SendMessageListToClients.DRF.serializers import AllowedSendingTimeSerializer


class LoadTimeZoneApiTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('super_user', 'super_user@user.com', 'super_user')
        self.test_user = User.objects.create(username='test_user', password='test_user')

        self.test_data = {"time_min": datetime.time(8), "time_max": datetime.time(20)}
        self.test_data_create = {"time_min": datetime.time(10), "time_max": datetime.time(20)}
        self.test_data_update = {"time_min": datetime.time(10), "time_max": datetime.time(22)}
        self.test_data_instance = AllowedSendingTime.objects.create(**self.test_data)

        self.url_list = reverse('allowedsendingtime-list')
        self.url_detail = reverse('allowedsendingtime-detail', args=[self.test_data_instance.id])

    def test_get_no_auth(self):
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_auth_user(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_auth_super_user(self):
        self.client.force_login(user=self.superuser)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get(self):
        self.client.force_login(user=self.test_user)
        response = self.client.get(self.url_list)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIsInstance(response.json(), list)

    def test_create(self):
        self.client.force_login(user=self.test_user)
        response = self.client.post(self.url_list, data=self.test_data_create, format='json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        response.json().pop('id')
        self.assertEqual(AllowedSendingTimeSerializer(self.test_data_create).data, response.json())

    def test_update(self):
        self.client.force_login(user=self.test_user)
        response = self.client.put(self.url_detail, data=self.test_data_update, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        response.json().pop('id')
        self.assertEqual(AllowedSendingTimeSerializer(self.test_data_update).data, response.json())

    def test_destroy(self):
        self.client.force_login(user=self.test_user)
        response = self.client.delete(self.url_detail)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
