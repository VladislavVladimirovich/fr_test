from django.contrib import admin
from SendMessageListToClients.models import *


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')


class TimeZoneAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'utcoffset')
    list_display_links = ('id', 'name')
    list_filter = ('utcoffset',)
    search_fields = ('name', )


class MobileCodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'code')
    list_display_links = ('id', 'code')


class StatusSendMessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'state')
    list_display_links = ('id', 'name', 'description')
    readonly_fields = ('id', 'name', 'state')

    def has_delete_permission(self, request, obj=None):
        """ Убрать кнопку удаления """
        return False


class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'number_phone')
    list_display_links = ('id', 'number_phone')


class MailingMessageInline(admin.StackedInline):
    model = Message
    extra = 0
    readonly_fields = ('client', 'status_send', 'date_time')
    fields = (('client', 'status_send', 'date_time'), )


class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'email_statistic', 'tags', 'mobile_codes', 'message_text', 'date_time_start', 'date_time_stop')
    list_display_links = ('id', 'tags', 'mobile_codes', 'message_text')
    inlines = (MailingMessageInline,)
    save_on_top = True
    fields = (
        ('email_statistic', 'mobile_code', 'tag'),
        ("date_time_start", "date_time_stop", "allowed_sending_time"),
        ('message_text',)
    )
    search_fields = ('message_text', 'mobile_codes')

    def tags(self, obj):
        data = ','.join(obj.tag.all().values_list('name', flat=True))
        return self.__cut_str(data)

    def mobile_codes(self, obj):
        data = ','.join([str(x) for x in obj.mobile_code.all().values_list('code', flat=True)])
        return self.__cut_str(data)

    @staticmethod
    def __cut_str(data: str, len_: int = 40) -> str:
        return data if len(data) <= len_ else data[:len_] + '...'


class AllowedSendingTimeAdmin(admin.ModelAdmin):
    list_display = ('id', 'time_min', 'time_max')


admin.site.register(Tag, TagAdmin)
admin.site.register(TimeZone, TimeZoneAdmin)
admin.site.register(MobileCode, MobileCodeAdmin)
admin.site.register(StatusSendMessage, StatusSendMessageAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(AllowedSendingTime, AllowedSendingTimeAdmin)