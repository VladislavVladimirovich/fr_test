import logging
from django.conf import settings as django_settings

from ..models import *


logger_err = logging.getLogger(django_settings.LOGGER_NAMES['ERR'])


STATUS = [
    {"id": 1, "name": "New_message", "description": "Новое сообщение", "state": False},
    {"id": 2, "name": "Error", "description": "Ошибка отправки сообщения", "state": False},
    {"id": 3, "name": "Processing", "description": "В процессе отправки", "state": False},
    {"id": 4, "name": "Successful", "description": "Успешно доставлено", "state": True},
    {"id": 5, "name": "ErrorTimeOut", "description": "Истекло время отправки", "state": False},
    {"id": 6, "name": "BanAllowedTime", "description": "В данное время запрещена отправка сообщений", "state": False},

]


def cheсk_data():

    instances = list(StatusSendMessage.objects.filter(name__in=[i['name'] for i in STATUS]))

    for i in instances:
        [STATUS.pop(key) for key, j in enumerate(STATUS) if i.name == j['name']]

    return STATUS


def create_status_message():
    try:
        data_status = cheсk_data()
        data_create = [StatusSendMessage(**i) for i in data_status]
        if data_create:
            StatusSendMessage.objects.bulk_create(data_create)
        return "successful"
    except Exception as exc:
        logger_err.error({'message': str(exc), 'handler': 'create_status_message'})
        return False
