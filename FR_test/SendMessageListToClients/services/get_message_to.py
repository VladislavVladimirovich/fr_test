import pytz
import logging

from datetime import datetime

from django.db.models import Q, F
from django.utils import timezone
from django.conf import settings as django_settings

from ..models import *


logger = logging.getLogger(django_settings.LOGGER_NAMES['SEND_SMS'])
logger_err = logging.getLogger(django_settings.LOGGER_NAMES['ERR'])


class FiltersMessagesTo:

    @staticmethod
    def start_stop_time(stop_t) -> bool:
        """ Поверка диапазона даты и времени, начала и окончания рассылки с учетом временной зоны клиента"""
        return timezone.localtime() < stop_t

    def allowed_time_send(self, instance) -> None | datetime:
        # Текущее время больше времени старт и входит в диапазон разрешенного времени
        if timezone.localtime() > instance['date_time_start'] and self.__allowed_time(instance, timezone.localtime()):
            return instance['date_time_start']

        date_start = self.set_time_zone(instance['date_time_start'], instance['tag__client_rel__time_zone__name'])
        # Текущее время меньше времени старт, время старт входит в диапазон разрешенного времени
        if timezone.localtime() < instance['date_time_start'] and self.__allowed_time(instance, date_start):
            return date_start
        return None

    @staticmethod
    def create_or_update_messages(messages: list):
        Create_, Update_ = list(), list()
        for message_ in messages:
            Update_.append(message_) if message_.id else Create_.append(message_)
        Message.objects.bulk_create(Create_)
        Message.objects.bulk_update(Update_, ['status_send_id', 'date_time'])
        return [i for i in Create_ + Update_ if i.status_send_id not in [5, 6]]

    @staticmethod
    def set_time_zone(date_time_: datetime, time_zone_: str) -> datetime:
        return pytz.timezone(time_zone_).localize(date_time_.astimezone().replace(tzinfo=None))

    @staticmethod
    def __allowed_time(instance, time_):
        if not instance['allowed_sending_time__time_min'] or instance['allowed_sending_time__time_max']:
            return True
        return instance['allowed_sending_time__time_min'] < time_ < instance['allowed_sending_time__time_max']


class GetMessageTo:

    NEW_MESSAGE = 1
    ERROR = 2
    PROCESSING = 3
    SUCCESSFUL = 4
    ERRORTIMOUT = 5
    BANALLOWEDTIME = 6

    def get_message(self) -> list:
        messages = list()
        instances = self.__get_instances()

        for instance in instances:
            # Получение созданного сообщения
            message_ = self.__get_message(instance)
            if not message_:
                continue

            date_stop = FiltersMessagesTo.set_time_zone(
                instance['date_time_stop'], instance['tag__client_rel__time_zone__name']
            )

            # Проверить диапазон даты и времени запуска и окончания рассылки
            if not FiltersMessagesTo().start_stop_time(date_stop):
                logger.info({"id": message_.id, "status": self.ERRORTIMOUT})
                message_.status_send_id = self.ERRORTIMOUT
                # self.__set_status(instance, self.ERRORTIMOUT)
                messages.append(message_)
                continue

            date_start = FiltersMessagesTo().allowed_time_send(instance)
            # Проверить диапазон даты и времени разрешения отправки клиенту
            if not date_start:
                logger.info({"id": message_.id, "status": self.BANALLOWEDTIME})
                message_.status_send_id = self.BANALLOWEDTIME
                # self.__set_status(instance, self.BANALLOWEDTIME)
                messages.append(message_)
                continue

            message_.date_time = date_start
            messages.append(message_)

        result = FiltersMessagesTo.create_or_update_messages(messages)
        return [{'message_id': i.id, 'date_start': i.date_time} for i in result]

    def __get_instances(self):
        """ Получение сообщений для отправки клиентам из БД """
        list_required_statuses = [self.ERROR, self.BANALLOWEDTIME]
        fields = (
            'id',
            'message_text',
            'date_time_start',
            'date_time_stop',
            'tag__client_rel__id',
            'mobile_code__client_rel__id',
            'tag__client_rel__time_zone__name',
            'tag__client_rel__time_zone__utcoffset',
            'message_rel__id',
            'message_rel__status_send_id',
            'allowed_sending_time__time_max',
            'allowed_sending_time__time_min')

        new_message = list(Mailing.objects.filter(
            Q(message_rel__isnull=True) & Q(mobile_code__client_rel__id=F('tag__client_rel__id'))
        ).values(*fields))
        old_message = list(Mailing.objects.filter(
            Q(message_rel__status_send_id__in=list_required_statuses) &
            Q(
                Q(message_rel__client_id=F('tag__client_rel__id')) &
                Q(message_rel__client_id=F('mobile_code__client_rel__id'))
            )
        ).values(*fields))

        return new_message + old_message

    def __get_message(self, instance) -> Message | None:
        try:
            status_ = self.NEW_MESSAGE
            id_ = None
            if instance['message_rel__id']:
                status_ = instance['message_rel__status_send_id']
                id_ = instance['message_rel__id']
            return Message(
                id=id_,
                date_time=instance['date_time_start'],
                status_send_id=status_,
                mailing_id=instance['id'],
                client_id=instance['tag__client_rel__id']
            )
        except Exception as exc:
            logger_err.error(exc)
            return None

    @staticmethod
    def __set_status(instance, status):
        try:
            instance.status_send_id = status
            instance.save()
            return True
        except Exception as exc:
            logger_err.error(exc)
            return False
