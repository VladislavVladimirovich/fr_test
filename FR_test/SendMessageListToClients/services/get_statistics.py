import logging

from django.utils import timezone
from django.conf import settings as django_settings
from SendMessageListToClients.models import *


logger = logging.getLogger(django_settings.LOGGER_NAMES['ERR'])


def serializer_data(instance) -> dict:
    try:
        result = {
            'id': instance.id,
            'statistics': dict(),
            'message_text': instance.message_text,
            'date_time_start': str(timezone.localtime(instance.date_time_start)),
            'date_time_stop': str(timezone.localtime(instance.date_time_stop)),
            'tag': [{'id': i.id, 'name': i.name} for i in instance.tag.all()],
            'mobile_code': [{'id': i.id, 'code': i.code} for i in instance.mobile_code.all()]
        }
        count_msg = 0
        for i in instance.message_rel.all():
            statistics = result['statistics'].setdefault(i.status_send.name, dict())
            data = statistics.setdefault('data', list())
            data.append({
                'id': i.id,
                'date_time': str(timezone.localtime(i.date_time)),
                'client': {
                    'id': i.client.id,
                    'number_phone': i.client.number_phone,
                    'time_zone': {
                        'id': i.client.time_zone.id,
                        'name': i.client.time_zone.name,
                        'utcoffset': i.client.time_zone.utcoffset},
                    'tag': {
                        'id': i.client.tag.id,
                        'name': i.client.tag.name},
                    'mobile_code': {
                        'id': i.client.mobile_code.id,
                        'code': i.client.mobile_code.code},
                }
            })
            count_msg += 1
        result['statistics'].update({'count': count_msg})
    except Exception as exc:
        logger.error({'message': str(exc), "handler": "get_statistics.serializer_data"})
        return dict()

    return result


def get_statistics(today=None):
    try:
        result = list()
        filter_ = {"email_statistic": True} if today else dict()
        models = list(Mailing.objects.prefetch_related(
            'tag',
            'mobile_code',
            'message_rel__client__mobile_code',
            'message_rel__client__tag',
            'message_rel__status_send',
            'message_rel__client__time_zone',
        ).filter(**filter_))

        for i in models:
            i_dict = serializer_data(i)
            if i_dict:
                result.append(i_dict)

        return {"status": "successful", "data": result}
    except Exception as exc:
        logger.error({"message": str(exc), "handler": "get_statistics"})
        return {"status": "error"}
