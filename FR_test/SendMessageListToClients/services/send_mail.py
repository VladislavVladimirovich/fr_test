import json
import logging
import time

from django.core.mail import EmailMessage
from .get_statistics import get_statistics
from django.conf import settings


logger_info = logging.getLogger(settings.LOGGER_NAMES['INFO']).info
logger_err = logging.getLogger(settings.LOGGER_NAMES['ERR']).error


LOGGING_MESSAGES = [
    'Ошибка получения статистики',
    'Нет данных для отправки статистики',
    'Email получателей не установлены, отправка статистики не выполнена',
    'Email отправителя не установлен, отправка статистики не выполнена',
    'Ошибка валидации данных: ',
    'Ошибка валидации email: ',
    'Ошибка установки параметров сообщения: ',
    'Ошибка отправки статистики по адреса: ',
    'Отправка статистики выполнена успешна, по адресам: ',
]


def form_body(data: dict) -> str:
    result = dict()
    for i in data['data']:
        result.update({f"mailing_id_{i['id']}": {
            'messages_count': i['statistics']['count'],
            'text': i['message_text'],
            'statistics': {key: len(val['data']) for key, val in i['statistics'].items() if not key == 'count'},
        }})
    return json.dumps(result, ensure_ascii=False, indent=4)


class ValidatorDataMessage:
    def __init__(self, body, email_to, email_from):
        self.errors = list()
        self.body = body
        self.email_to = email_to
        self.email_from = email_from
        self.file_name = None
        self.title_message = None

    def handle(self):
        self.__body()
        self.__email()
        self.__set_param()

    def __body(self):
        try:
            if not self.body['status'] == "successful":
                self.errors.append((LOGGING_MESSAGES[0], logger_info))
                return

            if not self.body['data']:
                self.errors.append((LOGGING_MESSAGES[1], logger_info))
                return
        except Exception as exc:
            self.errors.append((LOGGING_MESSAGES[4] + str(exc), logger_err))

    def __email(self):
        try:
            self.email_to = [i.replace(' ', '') for i in self.email_to.split(',') if i not in ['', ' ']]
            if not self.email_to:
                self.errors.append((LOGGING_MESSAGES[2], logger_info))

            if not self.email_from:
                self.errors.append((LOGGING_MESSAGES[3], logger_info))
        except Exception as exc:
            self.errors.append((LOGGING_MESSAGES[5] + str(exc), logger_err))

    def __set_param(self):
        try:
            self.file_name = f"{time.strftime('%Y%m%d', time.localtime(time.time()))}_statistics_mailing.json"
            self.title_message = str(settings.BASE_DIR).split('/')[-1] + " - service, статистика по рассылкам."
        except Exception as exc:
            self.errors.append((LOGGING_MESSAGES[6] + str(exc), logger_err))

    def is_valid(self):
        return not bool(self.errors)

    def write_log(self, messages: list = None, logger: logging = logger_info, handler='mail_send'):
        for i in messages or self.errors:
            logger({'message': i, 'handler': handler}) if messages else i[1]({'message': i[0], 'handler': handler})


def mail_send():
    data = get_statistics(True)
    validator = ValidatorDataMessage(data, settings.RECEIVERS, settings.EMAIL_HOST_USER)
    validator.handle()
    if not validator.is_valid():
        validator.write_log()
        return list()

    try:
        mail = EmailMessage(
            subject=validator.title_message,
            body=form_body(validator.body),
            to=validator.email_to,
            from_email=validator.email_from,
        )
        mail.attach(
            validator.file_name,
            json.dumps(get_statistics(True), ensure_ascii=True, indent=4),
            'application/json'
        )

        if not mail.send():
            validator.write_log([LOGGING_MESSAGES[7] + settings.RECEIVERS], logger_err)
            return list()

    except Exception as exc:
        validator.write_log([str(exc)], logger_err)
        return list()

    validator.write_log([LOGGING_MESSAGES[8] + settings.RECEIVERS], logger_info)
    return settings.RECEIVERS
