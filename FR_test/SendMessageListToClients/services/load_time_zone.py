import requests
from django.db import IntegrityError
from rest_framework import status

from SendMessageListToClients.models import TimeZone
from bs4 import BeautifulSoup


def pars_html(html: str) -> list:
    soup = BeautifulSoup(html, 'lxml')
    table = soup.findAll(name='table', attrs={"class":"wikitable"})[1]
    tr = table.find('tbody').findAll(name='tr')
    return [(i.findAll('td')[2].a['title'], i.findAll('td')[4].text.replace('\n', '')) for i in tr if i.findAll('td')]


def set_to_db(data: list):
    try:
        TimeZone.objects.bulk_create([TimeZone(name=i[0], utcoffset=i[1]) for i in data])
    except IntegrityError as exc:
        return "Time_zone были получены ранее"

    return "successful"


def load_time_zone():
    url = 'https://en.wikipedia.org/wiki/Time_in_Russia'
    resp = requests.get(url)

    if not resp.status_code == status.HTTP_200_OK:
        return F"Connection url {url} error, <status_code:{resp.status_code}>"

    set_to_db(pars_html(resp.text))
    return set_to_db(pars_html(resp.text))



