import requests
import logging

from celery import shared_task

from django.utils import timezone
from django.conf import settings as django_settings

from .models import *

from .services import GetMessageTo, send_mail


logger = logging.getLogger(django_settings.LOGGER_NAMES['SEND_SMS'])


# OLD
# @shared_task
# def Create_message():
#     """ Отправка сообщений пользователям """
#     count_data = 0
#     messages_data = list(Mailing.objects.filter(
#         Q(message_rel__isnull=True) | Q(message_rel__status_send_id__in=[2, 6])
#     ).values(
#         'id',
#         'message_text',
#         'date_time_start',
#         'date_time_stop',
#         'tag__client_rel__id',
#         'mobile_code__client_rel__id',
#         'tag__client_rel__time_zone__name',
#         'message_rel__id',
#         'message_rel__status_send_id',
#         'allowed_sending_time__time_max',
#         'allowed_sending_time__time_min',
#     ))
#     for i in messages_data:
#         if i["tag__client_rel__id"] and i['mobile_code__client_rel__id'] == i["tag__client_rel__id"]:
#             # set time zone
#             start_time_client = set_time_zone(i['date_time_start'], i['tag__client_rel__time_zone__name'])
#             stop_time_client = set_time_zone(i['date_time_stop'], i['tag__client_rel__time_zone__name'])
#
#             if i['message_rel__id'] is None:
#                 i['message_rel__id'] = Message.objects.create(
#                     date_time=start_time_client,
#                     status_send_id=1,   # set status New_message
#                     mailing_id=i['id'],
#                     client_id=i['tag__client_rel__id']
#                 ).id
#                 # logger.info({"id": i['message_rel__id'], "status": 1})
#             if timezone.localtime() < stop_time_client:
#                 allowed_time = {
#                     'min': i['allowed_sending_time__time_min'],
#                     'max': i['allowed_sending_time__time_max'],
#                     'time_zone': i['tag__client_rel__time_zone__name']
#                 }
#                 if timezone.localtime() > start_time_client:
#                     send_message.delay(i['message_rel__id'], allowed_time)
#                 else:
#                     send_message.apply_async((i['message_rel__id'], allowed_time), eta=start_time_client)
#             else:
#                 if not i['message_rel__status_send_id'] == 4:
#                     # set status ErrorTimeOut
#                     Message.objects.update(status_send_id=5)
#                     # logger.info({"id": i['message_rel__id'], "status": 5})
#             count_data += 1
#
#     return F"Count of processed tasks {count_data}"
@shared_task
def Send_mail():
    return send_mail()


@shared_task
def Create_message():
    for message_ in GetMessageTo().get_message():
        if timezone.localtime() > message_['date_start']:
            send_message.delay(message_['message_id'])
        else:
            send_message.apply_async((message_['message_id'],), eta=message_['date_start'])


@shared_task
def send_message(sms_id: int) -> Exception | str:
    url = 'http://127.0.0.1:8000/test/'
    headers = {"Accept": "application/json"}
    instance = Message.objects.get(pk=sms_id)
    data = {
        "id": instance.id,
        "phone": instance.client.number_phone,
        "text": instance.mailing.message_text
    }

    # set status Processing
    instance.status_send_id = 3
    logger.info({"id": instance.id, "status": 3})
    instance.save()
    try:
        resp = requests.post(url, data=data, headers=headers, timeout=10)
        if not resp.status_code == 200:
            raise ValueError(f"Error send <status_code{resp.status_code}>")
    except Exception as exc:
        # set status Error
        instance.status_send_id = 2
        instance.save()
        logger.info({"id": instance.id, "message": str(exc), "status": 2})
        return str(exc)

    # set status Successful
    instance.status_send_id = 4
    instance.save()

    logger.info({"id": instance.id, "message": "Successful", "status": 4})
    return f'Send message <id:{instance.id}>'
