from django.core.validators import MinLengthValidator
from django.db.models import Model, CharField, IntegerField
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django_prometheus.models import ExportModelOperationsMixin


class Client(ExportModelOperationsMixin('Client'), Model):
    number_phone = CharField(max_length=11, unique=True, validators=[MinLengthValidator(11)])
    mobile_code = ForeignKey('MobileCode', on_delete=CASCADE, related_name='client_rel')
    tag = ForeignKey('Tag', on_delete=CASCADE, related_name='client_rel')
    time_zone = ForeignKey('TimeZone', on_delete=CASCADE, related_name='client_rel')

    class Meta:
        db_table = "client"
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return f'id={self.pk}, number_phone={self.number_phone}'


class MobileCode(ExportModelOperationsMixin('MobileCode'), Model):
    code = IntegerField()

    class Meta:
        db_table = "mobile_code"
        verbose_name = "Код мобильного оператора"
        verbose_name_plural = "Коды мобильных операторов"

    def __str__(self):
        return f'id={self.id}, code={self.code}'


class TimeZone(ExportModelOperationsMixin('TimeZone'), Model):
    name = CharField(max_length=40, unique=True)
    utcoffset = CharField(max_length=6, null=True, default=None)

    class Meta:
        db_table = "time_zone"
        verbose_name = "Временная зона"
        verbose_name_plural = "Временные зоны"

    def __str__(self):
        return f'id={self.id}, time_zone_name={self.name}'


class Tag(ExportModelOperationsMixin('Tag'), Model):
    name = CharField(max_length=180)

    class Meta:
        db_table = "tag"
        verbose_name = "Тег"
        verbose_name_plural = "Теги"

    def __str__(self):
        return f'id={self.id}, tag={self.name}'
