from django.utils import timezone
from django.db.models import Model, DateTimeField, CharField, BooleanField
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django_prometheus.models import ExportModelOperationsMixin


class Message(ExportModelOperationsMixin('Message'), Model):
    date_time = DateTimeField(default=timezone.localtime)
    status_send = ForeignKey('StatusSendMessage', on_delete=CASCADE, related_name='message_rel')
    mailing = ForeignKey('Mailing', on_delete=CASCADE, related_name='message_rel')
    client = ForeignKey('Client', on_delete=CASCADE, related_name='message_rel')

    class Meta:
        db_table = "message"
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"


class StatusSendMessage(ExportModelOperationsMixin('StatusSendMessage'), Model):
    name = CharField(max_length=20)
    description = CharField(max_length=120, null=True, default=True)
    state = BooleanField(default=False)

    class Meta:
        db_table = "status_send_message"
        verbose_name = "Статус отправки сообщения"
        verbose_name_plural = "Статусы отправки сообщений"

    def __str__(self):
        return f"name={self.name}, description={self.description}, state={self.state}"
