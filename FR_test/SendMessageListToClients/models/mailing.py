from django.utils import timezone
from django.db.models import Model, TextField, DateTimeField, TimeField, BooleanField
from django.db.models import ManyToManyField, ForeignKey
from django.db.models import SET_NULL
from django_prometheus.models import ExportModelOperationsMixin


class Mailing(ExportModelOperationsMixin('Mailing'), Model):
    mobile_code = ManyToManyField('MobileCode', related_name='mailing_rel')
    tag = ManyToManyField('Tag', related_name='mailing_rel')
    message_text = TextField()
    date_time_start = DateTimeField(default=timezone.localtime)
    date_time_stop = DateTimeField()
    allowed_sending_time = ForeignKey('AllowedSendingTime', null=True, on_delete=SET_NULL, related_name='message_rel')
    email_statistic = BooleanField(default=False)

    class Meta:
        db_table = "mailing"
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return f'date_time_start={self.date_time_start}, ' \
               f'date_time_stop={self.date_time_stop}, ' \
               f'message_text={self.message_text}'


class AllowedSendingTime(ExportModelOperationsMixin('AllowedSendingTime'), Model):
    time_min = TimeField()
    time_max = TimeField()

    class Meta:
        db_table = "AllowedSendingTime"
        verbose_name = "Разрешенное время отправки сообщения"
        verbose_name_plural = "Разрешенное время отправки сообщений"

    def __str__(self):
        return f"id={self.id}, time_min={self.time_min}, time_max={self.time_max}"
