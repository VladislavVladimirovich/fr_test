from django.urls import path, include
from SendMessageListToClients.views import *

from rest_framework import routers


router = routers.SimpleRouter()
router.register(r'client', Client)
router.register(r'mailing', Mailing)
router.register(r'tag', Tag)
router.register(r'time_zone', TimeZone)
router.register(r'mobile_code', MobileCode)
router.register(r'status_send_message', StatusSendMessage)
router.register(r'allowed_sending_time', AllowedSendingTime)


urlpatterns = [
    path('load_time_zone/', SetTimeZone.as_view(), name='load_time_zone'),
    path('set_status_message/', SetStatusSendMessage.as_view(), name='set_status_message'),
    path('message/', Message.as_view(), name='message'),
    path('mailing/statistics/', Mailing.as_view({'get': 'statistics'})),
    path('mailing/<int:mailing_id>/messages/', Message.as_view()),
    path('message/<int:client_id>/', Message.as_view()),
    path('', include(router.urls)),
]
