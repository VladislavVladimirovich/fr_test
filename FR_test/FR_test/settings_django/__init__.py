try:
    from .developer import *
    print('Settings developer')
except ModuleNotFoundError:
    from .production import *
    print('Settings production')
