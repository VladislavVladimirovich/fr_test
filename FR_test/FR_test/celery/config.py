from kombu import Exchange, Queue

from FR_test.settings import TIME_ZONE
from FR_test.settings import REDIS_USER, REDIS_PASS, REDIS_HOST, REDIS_PORT
from FR_test.settings import REDIS_DB_CELERY_BACKEND
from FR_test.settings import REDIS_DB_CELERY_BROKER
from FR_test.settings import REDIS_DB_CELERY_CACHE

from .register_tasks_cron import RegisterTasks


class DefaultConf:
    REDIS_URL = f'redis://{REDIS_USER}:{REDIS_PASS}@{REDIS_HOST}:{REDIS_PORT}/'
    CELERY_BROKER_URL = '{}{}'.format(REDIS_URL, REDIS_DB_CELERY_BROKER)
    result_backend = '{}{}'.format(REDIS_URL, REDIS_DB_CELERY_BACKEND)
    cache_backend = '{}{}'.format(REDIS_URL, REDIS_DB_CELERY_CACHE)
    CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
    CELERY_broker_transport_options = {'visibility_timeout': 3600}
    accept_content = ['application/json']
    task_serializer = 'json'
    result_serializer = 'json'
    timezone = TIME_ZONE


class CeleryQueue:
    task_queues = (
        Queue('main', Exchange('main'), routing_key='main'),
        Queue('send_message', Exchange('send_message'), routing_key='send_message'),
    )
    CELERY_TASK_DEFAULT_QUEUE = 'main'
    CELERY_TASK_DEFAULT_EXCHANGE = 'main'
    CELERY_TASK_DEFAULT_ROUTING_KEY = 'main'
    CELERY_TASK_ROUTES = {
        'SendMessageListToClients.tasks.send_message': {'queue': 'send_message', 'routing_key': 'send_message'},
        'SendMessageListToClients.tasks.Create_message': {'queue': 'send_message', 'routing_key': 'send_message'},
    }


class CeleryConfig(DefaultConf, CeleryQueue):
    CELERY_BEAT_SCHEDULE = RegisterTasks.get_tasks()
