import os
from celery import Celery

from .config import CeleryConfig


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'FR_test.settings')

celery_app = Celery('FR_test')
celery_app.config_from_object(CeleryConfig, namespace='CELERY')
celery_app.autodiscover_tasks(['FR_test', 'SendMessageListToClients'])
