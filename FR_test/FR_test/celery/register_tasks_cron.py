import os
from pathlib import Path

from celery.schedules import crontab
from dotenv import load_dotenv


env_path = Path('../.env')
load_dotenv(dotenv_path=env_path)


def get_time_send_statistic():
    try:
        data_time = list(map(int, os.getenv('EMAIL_TIME_SEND').split(':')))
        if data_time[0] < 0 or data_time[0] > 23:
            raise ValueError('Error hour format 0...23')
        if data_time[1] < 0 or data_time[1] > 60:
            raise ValueError('Error minute format 0...60')
        return data_time
    except:
        return 0, 0


class TaskCreatSendMail:
    """ Отправка статистики по почте """
    name = 'task.Send_mail'
    task = 'SendMessageListToClients.tasks.Send_mail'
    hour, minute = get_time_send_statistic()
    schedule = crontab(minute=minute, hour=hour)
    options = {'queue': 'main'}


class TaskCreatMessage:
    """ Отправка сообщений """
    name = 'task.Create_message'
    task = 'SendMessageListToClients.tasks.Create_message'
    schedule = crontab(minute='*/1', )


class RegisterTasks:

    @staticmethod
    def get_tasks():
        result = dict()
        names = [i for i in globals() if i.startswith('Task')]
        for i in names:
            obj = eval(i)
            args = [attribute for attribute in dir(obj) if attribute.startswith('__') is False]
            name = obj.__dict__.get('name') or i
            result.update({name: {arg: obj.__dict__[arg] for arg in args if not arg == 'name'}})
        return result
