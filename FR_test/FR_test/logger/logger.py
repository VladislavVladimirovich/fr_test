import os
import time

from .formaters import CustomJsonFormatter


rq = time.strftime('%Y%m%d', time.localtime(time.time()))
BASE_DIR = os.getcwd()
BASE_LOG_DIR_INFO = os.path.join(BASE_DIR, "log/info")
BASE_LOG_DIR_ERR = os.path.join(BASE_DIR, "log/err")
BASE_LOG_DIR_SEN_MESSAGE = os.path.join(BASE_DIR, "log/send_message")

os.makedirs(BASE_LOG_DIR_INFO, exist_ok=True)
os.makedirs(BASE_LOG_DIR_ERR, exist_ok=True)
os.makedirs(BASE_LOG_DIR_SEN_MESSAGE, exist_ok=True)


def Logger(names, levels):
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': '[{levelname}] | {asctime} | [{module}] : {message}',
                'style': '{',
            },
            'json': {
                '()': CustomJsonFormatter,
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
            },
            'django_info': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'maxBytes': 1024 * 1024 * 25,   # размер журнала 5M
                'filename': os.path.join(BASE_LOG_DIR_INFO, f"{rq}_info.log"),
                'formatter': 'json',
            },
            'django_err': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'maxBytes': 1024 * 1024 * 25,   # размер журнала 5M
                'filename': os.path.join(BASE_LOG_DIR_ERR, f"{rq}_err.log"),
                'formatter': 'json'
            },
            'send_sms': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'maxBytes': 1024 * 1024 * 25,   # размер журнала 5M
                'filename': os.path.join(BASE_LOG_DIR_SEN_MESSAGE, f"{rq}.log"),
                'formatter': 'json'
            },
        },
        'loggers': {
            names['INFO']: {
                'handlers': ['console', 'django_info'],
                'level': levels['INFO'],
            },
            names['ERR']: {
                'handlers': ['console', 'django_err'],
                'level': levels['ERR'],
            },
            names['SEND_SMS']: {
                'handlers': ['console', 'send_sms'],
                'level': levels['SEND_SMS'],
            },
        }
    }
