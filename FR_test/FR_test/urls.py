"""FR_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from SendMessageListToClients.views import *
from django.conf import settings
from django.conf.urls.static import static
from django_prometheus import exports


urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('auth/', include('rest_framework.urls', namespace='restf')),
    path('', include('social_django.urls', namespace='social')),
    re_path(r'^login/?$', Auth.as_view(), name='login'),
    path('docs/', include(('SendMessageListToClients.yasg', 'yasg'), namespace='doc')),
    path('api/v1/', include('SendMessageListToClients.urls')),
    re_path(r'^prometheus/metrics/?$', exports.ExportToDjangoView, name="prometheus-django-metrics")
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
