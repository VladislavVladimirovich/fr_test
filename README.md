# Тестовое задание "ООО Фабрика Решений"
<img src="https://img.shields.io/badge/python-3776AB?style=for-the-badge&logo=Python&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/celery-37814A?style=for-the-badge&logo=Celery&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/django-092E20?style=for-the-badge&logo=Django&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/PostgreSQL-4169E1?style=for-the-badge&logo=PostgreSQL&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/Redis-DC382D?style=for-the-badge&logo=Redis&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/GitLab-4d2e00?style=for-the-badge&logo=GitLab&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=Docker&logoColor=ffffff"/>
<img src="https://img.shields.io/badge/Swagger-85EA2D?style=for-the-badge&logo=Swagger&logoColor=000000"/>
<img src="https://img.shields.io/badge/Git-F05032?style=for-the-badge&logo=Git&logoColor=ffffff"/>

[Тестовое задание для кандидатов-разработчиков](https://www.craft.do/s/n6OVYFVUpq0o6L)
### Запуск проекта:
+ [скачать проект](https://gitlab.com/VladislavVladimirovich/fr_test/-/archive/master/fr_test-master.zip)
+ запустить docker-compose (docker-compose up -d --build)
+ проект будет доступен localhost:8100 :
    >-  localhost:8100/admin/ - админ панель
    >-  localhost:8100/login/ - страница авторизации
    >-  localhost:8100/docs/ - swagger
    >-  localhost:8100/docs/redoc - swagger/redoc
    >-  localhost:8100/api/v1/ - url api
    >-  localhost:8100/prometheus/metrics/ - метрики prometheus
    >-  localhost:8102 - postgresql
    >-  localhost:8103 - redis
    >-  localhost:8104 - adminer
    >-  localhost:8105 - flower
+ api доступно после авторизации, если проект был запущен через docker-compose, будет создан superuser name=admin, pass=admin. 
Также будут заполнены сущности TimeZone и StatusSendMessage, данные для них хранятся в ./data/data_to_db/...json, если 
будет необходимость перезаписать эти данные, существуют два метода GET /api/v1/(SetTimeZone, SetStatusSendMessage), доступ к ним ограничен superuser

### Дополнительные задания:
1. организовать тестирование написанного кода 
    > тестирование проекта описано в .gitlab-ci.yml
2. обеспечить автоматическую сборку/тестирование с помощью GitLab CI
    > ./.gitlab-ci.yml
3. подготовить docker-compose для запуска всех сервисов проекта одной командой
    > ./docker-compose.yaml
4. none
5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI
    > http://site/docs/ - (swagger), http://site/docs/redoc/
6. реализовать администраторский Web UI 
    > http://site/admin
7. обеспечить интеграцию с внешним OAuth2 
    >- для проверка в ./.env установить свои данные для параметров:
    >>- GitLab
    >>- SOCIAL_AUTH_GITLAB_SECRET=\__your_secret_key\__
    >>- SOCIAL_AUTH_GITLAB_KEY=\__your_key\__
    >>- GitHub
    >>- SOCIAL_AUTH_GITHUB_SECRET=\__your_secret_key\__
    >>- SOCIAL_AUTH_GITHUB_KEY=\__your_key\__
    >- после установки параметров на странице http://site/login/ можно будет протестировать интеграцию OAuth2
8. реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
    > для проверка в ./.env установить свои данные для параметров:
    >> - EMAIL_TIME_SEND=\__time\__
    >>> время в которое будет осуществляться отправка статистики: 15:00 - каждый день в 15 часов
    >> - EMAIL_HOST_USER=\__email\__ , EMAIL_HOST_PASSWORD=\__email_password\__ 
    >>> email с которого будет происходить отправка статистики
    >> - EMAIL_RECEIVERS=
    >>> список через "," email адресов на которые будет отправляться статистика: mail_1@mail.com, mail_2@mail.com 
    >> - EMAIL_HOST, EMAIL_PORT
    >>> smtp host и port
9. удаленный сервис может быть недоступен, долго отвечать на запросы
10. реализовать отдачу метрик в формате prometheus
    > http://site/prometheus/metrics/
11. реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал"
    > при создании "рассылка", указать  id сущность allowed_sending_time
12. обеспечить подробное логирование на всех этапах обработки запросов
    >- ./FR-test/log - пишется статистика работы, для отключения указать в ./.env уровень логирования 'critical'
    >- http://site/admin/drf_api_logger/apilogsmodel/
    >- http://site:8105/tasks - история выполнения tasks отправки рассылок и статистики